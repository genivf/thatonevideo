package thatonevideo

import (
	"bytes"
	"html/template"
	"sort"
)

func Build(r *Recaller) ([]byte, error) {
	channels, err := r.GetChannels()
	if err != nil {
		return nil, err
	}

	videos, err := r.GetVideos()
	if err != nil {
		return nil, err
	}
	sort.Sort(channels)
	sort.Sort(videos)

	t, err := template.New("output").Parse(tmpl)
	if err != nil {
		return nil, err
	}

	buf := &bytes.Buffer{}
	if err := t.Execute(buf, struct {
		Channels Recalled
		Videos   Recalled
	}{channels, videos}); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

const tmpl = `
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<script type="text/javascript">
function timeAgo(time){
  var units = [
    { name: "second", limit: 60, in_seconds: 1 },
    { name: "minute", limit: 3600, in_seconds: 60 },
    { name: "hour", limit: 86400, in_seconds: 3600  },
    { name: "day", limit: 604800, in_seconds: 86400 },
    { name: "week", limit: 2629743, in_seconds: 604800  },
    { name: "month", limit: 31556926, in_seconds: 2629743 },
    { name: "year", limit: null, in_seconds: 31556926 }
  ];
  var diff = (new Date() - new Date(time*1000)) / 1000;
  if (diff < 5) return "now";
  
  var i = 0, unit;
  while (unit = units[i++]) {
    if (diff < unit.limit || !unit.limit){
      var diff =  Math.floor(diff / unit.in_seconds);
      return diff + " " + unit.name + (diff>1 ? "s" : "");
    }
  };
}

window.onload = function(){
	var times = document.getElementsByClassName("time");
	[].forEach.call(times, function(el){
		el.innerHTML = timeAgo(el.innerHTML) + " ago";
	});

	var channels = document.getElementById("channels");
	var videos = document.getElementById("videos");

	document.getElementById("channels_link").onclick = function() {
		channels.style.display = "block";
		videos.style.display = "none";
	};

	document.getElementById("videos_link").onclick = function() {
		videos.style.display = "block";
		channels.style.display = "none";
	};

	channels.style.display = "none";
};

</script>
<style type="text/css">
body { background: #333333; }
a { color: white; }
a:visited{ color: #9B80A1; }
#container {
	width: 890px;
	margin: auto;
	background: black;
	color: white;
	padding: 0.4em 1em;
}
.entry>* { display:inline; }
.time { float: right; }
.tabs {
	margin: 0;
	padding: 0;
	list-style: none;
	display: table;
	table-layout: fixed;
	width: 100%;
}
.tabs_item { display: table-cell; }
.tabs_link { display: block; }
.primary-nav {
    text-align: center;
    border-radius: 4px;
    overflow: hidden;
}
.primary-nav a {
    padding: 0.5em;
    background-color: #0f4478;
    color: #fff;
    font-weight: bold;
    text-decoration: none;
}
.primary-nav a:hover { background-color: #8f44a2; }
.primary-nav a:visited { color: #fff; }
</style>
<body>
<div id="container">
<div id="tab_bar">
	<ul class="tabs primary-nav">
	    <li class="tabs_item">
	        <a href="#" id="videos_link" class="tabs_link">Videos</a>
	    </li>
	    <li class="tabs_item">
	        <a href="#" id="channels_link" class="tabs_link">Channels</a>
	    </li>
	</ul>
</div>

<div id="videos">
{{ with .Videos }}
	{{ range $value := . }}
		<div class="entry">
			<div class="time">{{ $value.Added }}</div>
			<div class="link">
				<a href="http://youtube.com/watch?v={{ $value.Name }}">
					{{ $value.Title }}
				</a>
			</div>
		</div>
	{{ end }}
{{ end }}
</div>

<div id="channels">
{{ with .Channels }}
	{{ range $value := . }}
		<div class="entry">
			<div class="time">{{ $value.Added }}</div>
			<div class="link">
				<a href="http://youtube.com/channel/{{ $value.ID }}">
					{{ $value.Title }}
				</a>
			</div>
		</div>
	{{ end }}
{{ end }}
</div>
</div>
</body>
`
