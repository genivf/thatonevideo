package thatonevideo

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/boltdb/bolt"
)

type Recaller struct {
	db *bolt.DB
	yt *YoutubeClient
}

func New(dbPath string) *Recaller {
	db, err := bolt.Open(dbPath, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		log.Fatalln(err)
	}

	db.Batch(func(tx *bolt.Tx) error {
		for _, b := range []string{"videos", "channels"} {
			tx.CreateBucketIfNotExists([]byte(b))
		}
		return nil
	})

	key := GetKey("YOUTUBE_API_KEY")
	return &Recaller{
		db: db,
		yt: NewYoutubeClient(key),
	}
}

func (r *Recaller) Close() {
	r.db.Close()
}

type Whatwasit struct {
	Name  string
	Added int64
	*Item
}

type Recalled []*Whatwasit

func (r Recalled) Len() int           { return len(r) }
func (r Recalled) Swap(i, j int)      { r[i], r[j] = r[j], r[i] }
func (r Recalled) Less(i, j int) bool { return r[i].Added > r[j].Added }

func (r *Recaller) add(bucket string, link string, lookup func(s string) (*Item, string, error)) error {
	item, id, err := lookup(link)
	if err != nil {
		return err
	}

	return r.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		if b.Get([]byte(id)) != nil {
			return fmt.Errorf("link exists: %s", link)
		}

		buf, err := json.Marshal(Whatwasit{Name: id, Added: time.Now().Unix(), Item: item})
		if err != nil {
			return err
		}
		return b.Put([]byte(id), buf)
	})
}

func (r *Recaller) get(bucket string) (Recalled, error) {
	result := make(Recalled, 0)
	return result, r.db.View(func(tx *bolt.Tx) error {
		c := tx.Bucket([]byte(bucket)).Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			var item Whatwasit
			if err := json.Unmarshal(v, &item); err != nil {
				return err
			}
			result = append(result, &item)
		}
		return nil
	})
}

func (r *Recaller) remove(bucket string, id string) error {
	return r.db.Update(func(tx *bolt.Tx) error {
		return tx.Bucket([]byte(bucket)).Delete([]byte(id))
	})
}

func (r *Recaller) AddVideo(link string) error {
	return r.add("videos", link,
		func(s string) (*Item, string, error) {
			items, err := r.yt.LookupVideos(s)
			if err != nil {
				return nil, "", err
			}
			if len(items) < 1 {
				return nil, "", fmt.Errorf("no video found for: %s", link)
			}
			return items[0], items[0].ID, nil
		})
}

func (r *Recaller) GetVideos() (Recalled, error) {
	return r.get("videos")
}

func (r *Recaller) RemoveVideo(id string) error {
	return r.remove("videos", id)
}

func (r *Recaller) AddChannel(link string, user bool) error {
	return r.add("channels", link,
		func(s string) (*Item, string, error) {
			var items []*Item
			var err error
			if user {
				items, err = r.yt.LookupChannelsByName(s)
			} else {
				items, err = r.yt.LookupChannels(s)
			}
			if err != nil {
				return nil, "", err
			}
			if len(items) < 1 {
				return nil, "", fmt.Errorf("no channel found for: %s", link)
			}
			return items[0], items[0].ID, nil
		})
}

func (r *Recaller) GetChannels() (Recalled, error) {
	return r.get("channels")
}

func (r *Recaller) RemoveChannel(id string) error {
	return r.remove("channels", id)
}
