package thatonevideo

import (
	"encoding/json"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

func GetKey(env string) string {
	return os.Getenv(env)
}

type Item struct {
	ID       string
	Etag     string
	*Snippet `json:"Snippet"`
}

type Snippet struct {
	ChannelID    string
	Title        string
	ChannelTitle string
	Description  string
	PublishedAt  time.Time
}

type YoutubeClient struct{ apiKey string }

func NewYoutubeClient(apiKey string) *YoutubeClient {
	return &YoutubeClient{apiKey: apiKey}
}

func (y *YoutubeClient) get(ep string, kv map[string]string, data interface{}) error {
	const baseUri = "https://www.googleapis.com/youtube/v3/"

	query := url.Values{}
	query.Add("key", y.apiKey)
	for k, v := range kv {
		query.Add(k, v)
	}

	uri, err := url.Parse(baseUri + ep + "?" + query.Encode())
	if err != nil {
		return err
	}

	resp, err := http.Get(uri.String())
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	return json.NewDecoder(resp.Body).Decode(data)
}

func (y *YoutubeClient) LookupVideos(ids ...string) ([]*Item, error) {
	var vids = &struct{ Items []*Item }{}
	if err := y.get("videos", map[string]string{
		"id":     strings.Join(ids, ","),
		"part":   "snippet",
		"fields": "items(id, snippet(title, channelTitle, channelId, publishedAt))",
	}, &vids); err != nil {
		return nil, err
	}
	return vids.Items, nil
}

func (y *YoutubeClient) LookupChannels(ids ...string) ([]*Item, error) {
	var vids = &struct{ Items []*Item }{}
	if err := y.get("channels", map[string]string{
		"id":     strings.Join(ids, ","),
		"part":   "snippet",
		"fields": "items(etag, id, snippet(title, description, publishedAt))",
	}, &vids); err != nil {
		return nil, err
	}
	return vids.Items, nil
}

func (y *YoutubeClient) LookupChannelsByName(name string) ([]*Item, error) {
	var vids = &struct{ Items []*Item }{}
	if err := y.get("channels", map[string]string{
		"forUsername": name,
		"part":        "snippet",
		"fields":      "items(etag, id, snippet(title, description, publishedAt))",
	}, &vids); err != nil {
		return nil, err
	}
	return vids.Items, nil
}
