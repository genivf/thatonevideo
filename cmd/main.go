package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"regexp"

	"bitbucket.org/genivf/thatonevideo"

	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	app = kingpin.New("thatonevideo", "recalls videos I've previously seen")

	add  = app.Command("add", "add a video").Default()
	link = add.Arg("link", "link to add").Required().String()

	list  = app.Command("list", "list everything")
	build = app.Command("build", "build static page")

	push = app.Command("push", "push output to repo")
)

var channelRe = regexp.MustCompile(`(?:youtu\.be\/|youtube.com\/(?:\S*(?:channel\/(?P<channel>[\w\-]+))|(?:user\/(?P<user>[\w\-]+))))`)
var videoRe = regexp.MustCompile(`(?:youtu\.be\/|youtube.com\/(?:\S*(?:v|video_id)=|v\/|e\/|embed\/))(?P<id>[\w\-]{11})`)

func parse(re *regexp.Regexp, s string) map[string]string {
	m := map[string]string{}
	g := re.SubexpNames()
	for i, n := range re.FindAllStringSubmatch(s, -1)[0] {
		m[g[i]] = n
	}
	return m
}

func main() {
	out := thatonevideo.GetKey("THATONEVIDEO_OUTPUT")
	dbPath := path.Join(out, "links.db")

	r := thatonevideo.New(dbPath)

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case add.FullCommand():
		if channelRe.MatchString(*link) {
			m := parse(channelRe, *link)
			if user, ok := m["user"]; ok && user != "" {
				if err := r.AddChannel(user, true); err != nil {
					log.Println("cannot add user:", err)
					os.Exit(1)
				}
			} else if channel, ok := m["channel"]; ok {
				if err := r.AddChannel(channel, false); err != nil {
					log.Println("cannot add channel:", err)
					os.Exit(1)
				}
			}
		} else if videoRe.MatchString(*link) {
			m := parse(videoRe, *link)
			if err := r.AddVideo(m["id"]); err != nil {
				log.Println("cannot add video:", err)
				os.Exit(1)
			}
		}

	case list.FullCommand():
		channels, err := r.GetChannels()
		if err != nil {
			log.Println("error getting channels:", err)
			os.Exit(1)
		}
		videos, err := r.GetVideos()
		if err != nil {
			log.Println("error getting videos:", err)
			os.Exit(1)
		}

		fmt.Println("channels:")
		for _, ch := range channels {
			fmt.Println(ch.Added, "http://youtube.com/channel/"+ch.ID, ch.Title)
		}

		fmt.Println("videos:")
		for _, ch := range videos {
			fmt.Println(ch.Added, "http://youtube.com/watch?v="+ch.ID, ch.Title)
		}
	case build.FullCommand():
		data, err := thatonevideo.Build(r)
		if err != nil {
			log.Println("error building output", err)
			os.Exit(1)
		}

		if err := ioutil.WriteFile(out+"/index.html", data, 0666); err != nil {
			log.Println("error writing output file", err)
			os.Exit(1)
		}

	case push.FullCommand():
		if err := os.Chdir(out); err != nil {
			log.Println("cannot change dir", err)
			os.Exit(1)
		}

		if err := os.Chdir(out); err != nil {
			fmt.Println("push: cwd", err)
			os.Exit(1)
		}

		if output, err := exec.Command("cmd", "/c", "git commit -a -m 'updated' && git push").Output(); err != nil {
			log.Println("cannot push files", err)
			os.Exit(1)
		} else {
			fmt.Println("push:", string(output))
		}
	}
}
